var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express!!' });
});

router.post('/', function(req, res, next) {
  res.render('login', { title: 'Express!!',username:req.body.username,password: req.body.password });
});
module.exports = router;
