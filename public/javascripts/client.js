var socket = io();
let name ;
let textarea = document.querySelector('#message');
let message_area = document.querySelector('.msg_card_body');

do{
	name = prompt('please enter your name');
}while(!name)

textarea.addEventListener('keyup',(e)=>{
	if(e.key === 'Enter'){
		sendMessage(e.target.value)
	}
});

//send message function
function sendMessage(msg){
	let message = {
		message : msg.trim(),
		username : name
	}

	appendMessage(message, 'outgoing');

	textarea.value = "";
	socket.emit('message',message);
} 

function appendMessage(msg, type){
	let maindiv =document.createElement('div');
	let time = formatAMPM(new Date);
	
	if(type == 'incoming'){
		maindiv.classList.add('d-flex', 'justify-content-start', 'mb-4'); 
		maindiv.innerHTML= "<div class='img_cont_msg'>"+

									"<img src='https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg' class='rounded-circle user_img_msg'>"+
								"</div>"+
								"<div class='msg_cotainer'>"+
								"<div><b>"+msg.username+"</b></div>"
									+msg.message+
									"<span class='msg_time'>"+time+"</span>"+
								"</div>";
							
	}else if(type == 'outgoing'){
		maindiv.classList.add('d-flex', 'justify-content-end','outgoing', 'mb-4');
	 maindiv.innerHTML = "<div class='msg_cotainer_send'>"+
								"<div><b>"+msg.username+"</b></div>"
									+msg.message+
									"<span class='msg_time_send'>"+time+"</span>"+
								"</div>"+
								"<div class='img_cont_msg'>"+
							"<img src='https://2.bp.blogspot.com/-8ytYF7cfPkQ/WkPe1-rtrcI/AAAAAAAAGqU/FGfTDVgkcIwmOTtjLka51vineFBExJuSACLcBGAs/s320/31.jpg' class='rounded-circle user_img_msg'>"+
								"</div>";
							
	}
	console.log(typeof(maindiv));
	message_area.appendChild(maindiv);
}

//received message

 socket.on('message', function(msg) {
        appendMessage(msg, 'incoming');
});

 function formatAMPM(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}